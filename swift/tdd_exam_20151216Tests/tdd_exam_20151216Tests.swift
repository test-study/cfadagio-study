//
//  tdd_exam_20151216Tests.swift
//  tdd_exam_20151216Tests
//
//  Created by 최혁준 on 12/16/15.
//  Copyright (c) 2015 최혁준. All rights reserved.
//

import UIKit
import XCTest
import Foundation

class tdd_exam_20151216Tests: XCTestCase {
    var group : PersonGroup?
    var people : [Person]? = []

    override func setUp() {     //데이터와 로직 통합
        super.setUp()

        createPeopleData();    //셋업에서 설교하지 않기_1
        
        initializePeopleGroup();    //셋업에서 설교하지 않기_2
    }
    
    func shouldRegistedUserInGroup(){
        var targetUser1 : Person = getFirstTargetUser()
        
        XCTAssertTrue(group!.searchPeople(targetUser1.name)!.name == targetUser1.name,
            "그룹에 대상자(이름 : \(targetUser1.name) 님이 등록되어 있어야 합니다.")
    }
    
    func shouldBroadCastEventToTargetUser(){    //꼭 1개 기능만 테스트하기_1
        var lunchPayUser : Person = getLunchPayUser()
        var targetUser1 : Person = getFirstTargetUser()
        var targetUser2 : Person = getSecondTargetUser()
        
        var lunchAmount = 5000
        
        lunchPayUser.payLunch(targetUser1.userNum, amount: lunchAmount)
        lunchPayUser.payLunch(targetUser2.userNum, amount: lunchAmount)
        
        XCTAssertEqual(targetUser1.events.count, eventCount(1),
            "정산 대상자의 이벤트 갯수가 \(eventCount(1))개 존재해야 합니다.")
        
        XCTAssertEqual(targetUser2.events[0].amount, amount(lunchAmount),
            "정산 대상자의 금액이 \(amount(lunchAmount))원이어야 합니다.")
    }
    
    func shouldBroadCastEventToLunchPayUser(){    //꼭 1개 기능만 테스트하기_2
        var lunchPayUser : Person = getLunchPayUser()
        var targetUser1 : Person = getFirstTargetUser()
        var targetUser2 : Person = getSecondTargetUser()
        
        var lunchAmount = 5000
        
        lunchPayUser.payLunch(targetUser1.userNum, amount: lunchAmount)
        lunchPayUser.payLunch(targetUser2.userNum, amount: lunchAmount)
        
        XCTAssertEqual(lunchPayUser.getWillReceiveAmount(), amount(lunchAmount) * eventCount(2),
            "점심값 계산한 사람은 총 \(amount(lunchAmount) * eventCount(2))원을 받아야 합니다.")
    }
    
    private func createPeopleData(){
        people!.append(Person(userNum: 1, name: "이희승", balance: 0))
        people!.append(Person(userNum: 2, name: "서동우", balance: 0))
        people!.append(Person(userNum: 3, name: "장정필", balance: 0))
        people!.append(Person(userNum: 4, name: "김형진", balance: 0))
        people!.append(Person(userNum: 5, name: "김지별", balance: 0))
        people!.append(Person(userNum: 6, name: "정상진", balance: 0))
        people!.append(Person(userNum: 7, name: "최혁준", balance: 0))
    }
    
    private func initializePeopleGroup(){
        group  = PersonGroup()
        
        group!.addPeople(people!)
    }
    

    private func getFirstTargetUser() -> Person{
        return people![0]
    }
    
    private func getSecondTargetUser() -> Person{
        return people![1]
    }
    
    private func getLunchPayUser() -> Person{
        return people![people!.count - 1]
    }
    
    private func eventCount(cnt : Int) -> Int{  //매직 넘버 쓰지 않기
        return cnt
    }
    
    private func amount(amt : Int) -> Int{  //매직 넘버 쓰지 않기
        return amt
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock() {
            // Put the code you want to measure the time of here.
        }
    }
}