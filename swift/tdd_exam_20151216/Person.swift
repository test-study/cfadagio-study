//
//  People.swift
//  tdd_exam_20151216
//
//  Created by 최혁준 on 12/16/15.
//  Copyright (c) 2015 최혁준. All rights reserved.
//

import Foundation

class Person {
    let userNum : Int
    let name : String
    var balance : Int
    var events : [LunchEvent]
    
    init(userNum : Int, name : String, balance : Int){
        self.userNum = userNum
        self.name = name
        self.balance = balance
        self.events = [LunchEvent]()
    }
    
    func addEvent(event : LunchEvent){
        self.events.append(event)
    }
    
    func payLunch(targetUserNum : Int, amount : Int){
        self.events.append(LunchEvent(eventType: EventType.WILL_RECEIVE, targetUserNum: targetUserNum, amount: amount))
        
        NSNotificationCenter
            .defaultCenter()
            .postNotificationName("ADJUST_I_WANT_MONEY",
                object: nil,
                userInfo : ["eventUserNum" : self.userNum, "targetUserNum" : targetUserNum, "amount" : amount ])
    }
    
    func getWillReceiveAmount() -> Int{
        var amount : Int = 0;
        
        for e in events{
            if(e.eventType == EventType.WILL_RECEIVE){
                amount += e.amount
            }
        }
            
        return amount;
    }
}
