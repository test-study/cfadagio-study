//
//  PeopleGroup.swift
//  tdd_exam_20151216
//
//  Created by 최혁준 on 12/16/15.
//  Copyright (c) 2015 최혁준. All rights reserved.
//

import Foundation

class PersonGroup{
    var persons : [Person]
    
    init(){
        persons = [Person]()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("paySomebody:"), name: "ADJUST_I_WANT_MONEY", object: nil)
    }
    
    func addPerson(people : Person){
        self.persons.append(people)
    }
    
    func addPeople(people : [Person]){
        for p in people{
            addPerson(p)
        }
    }
    
    func searchPeople(name : String) -> Person?{
        for p in persons{
            if p.name == name{
                return p
            }
        }
        
        return nil
    }
    
    dynamic func paySomebody(notification: NSNotification){
        let payInfo:Dictionary<String,Int> = notification.userInfo as Dictionary<String,Int>
        
        for p in persons{
            if(p.userNum == payInfo["targetUserNum"]){
                p.addEvent(LunchEvent(eventType: EventType.WILL_PAY, targetUserNum: payInfo["eventUserNum"]!, amount: payInfo["amount"]!))
                break
            }
        }
    }
}
