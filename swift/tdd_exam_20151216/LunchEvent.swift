//
//  LunchEvent.swift
//  tdd_exam_20151216
//
//  Created by 최혁준 on 12/16/15.
//  Copyright (c) 2015 최혁준. All rights reserved.
//

import Foundation

class LunchEvent{
    let eventType : EventType
    let targetUserNum : Int
    let amount : Int
    var state : EventState
    
    init(eventType : EventType, targetUserNum : Int, amount : Int){
        self.eventType = eventType
        self.targetUserNum = targetUserNum
        self.amount = amount
        self.state = EventState.STAND_BY
    }
    
    func setState(state : EventState){
        self.state = state
    }
}

enum EventType{
    case WILL_PAY, WILL_RECEIVE
}

enum EventState{
    case STAND_BY, COMPLETED, CANCELED
}