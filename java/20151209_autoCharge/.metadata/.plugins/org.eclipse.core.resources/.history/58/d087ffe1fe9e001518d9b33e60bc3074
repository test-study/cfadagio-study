package src;

import static org.junit.Assert.assertEquals;
import model.AutoChargeTestDaoImpl;

import org.junit.Test;

import static org.mockito.Mockito.*;
import autoCharge.code.ChargeEventResultCode;
import autoCharge.code.ChargeResultCode;
import autoCharge.model.ChargeResult;
import autoCharge.model.EstGamesCharger;
import autoCharge.model.EventChargeResult;
import autoCharge.service.AutoChargeEventServiceImpl;
import autoCharge.service.AutoChargeServiceImpl;

public class AutoChargeServiceTest {
	@Test
	public void closeTest(){
		AutoChargeServiceImpl target = new AutoChargeServiceImpl(new AutoChargeTestDaoImpl(false, false, 1), new EstGamesCharger());
		
		assertEquals(ChargeResultCode.CLOSE_AUTO_CHARGE, target.charge("cb1", 3612166, 10000, 500).getCode());
	}
	
	@Test
	public void shouldReturnFailWhenOverCharge() throws Exception{
		EstGamesCharger chargerMock = mock(EstGamesCharger.class);
		AutoChargeEventServiceImpl eventServiceMock = mock(AutoChargeEventServiceImpl.class);
		
		int overChargeUserNum = 3612166;
		
		when(chargerMock.charge("cb1", 3612166, 9000)).thenReturn(false);
		
		AutoChargeServiceImpl target = new AutoChargeServiceImpl(eventServiceMock, new AutoChargeTestDaoImpl(true, true, 1), chargerMock);
		
		ChargeResult result = target.charge("cb1", overChargeUserNum, 1000, 10000);
		
		verify(eventServiceMock, times(1));
		assertEquals(ChargeResultCode.OVER_CHARGE_IN_MONTH, result.getCode());
		assertEquals(0, result.getChargedCashAmount());
		assertEquals(0, result.getChargedCashBonusAmount());
	}
	
	@Test
	public void shouldReturnFailWhenEventError() throws Exception{
		AutoChargeEventServiceImpl eventServiceMock = mock(AutoChargeEventServiceImpl.class);
		int chargeUserNum = 3612166;
		EventChargeResult eventError = new EventChargeResult(ChargeEventResultCode.INVALID_PARAMETERS, 0);
		
		when(eventServiceMock.charge("cb1", chargeUserNum, 9000)).thenReturn(eventError);
		
		AutoChargeServiceImpl target = new AutoChargeServiceImpl(eventServiceMock, new AutoChargeTestDaoImpl(true, true, 1), new EstGamesCharger());
		
		ChargeResult result = target.charge("cb1", chargeUserNum, 1000, 10000);
		
		assertEquals(ChargeResultCode.ERROR_EVENT_CHARGE, result.getCode());
		assertEquals(0, result.getChargedCashAmount());
		assertEquals(0, result.getChargedCashBonusAmount());
	}
}
