package autoCharge.repository;

public interface AutoChargeDao {
	public boolean getAutoChargeMaster(String gameCode);
	public boolean getAutoChargeEventMaster(String gameCode);
	public int getEventRate(String gameCode);
}
