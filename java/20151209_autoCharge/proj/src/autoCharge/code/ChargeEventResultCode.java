package autoCharge.code;

public enum ChargeEventResultCode {
	SUCCESS, INVALID_PARAMETERS, CLOSE_EVENT
}
