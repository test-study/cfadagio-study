package autoCharge.code;

public enum CalcResultCode {
	AVAIL_CHARGE, ENOUGH_BALANCE, INVALID_PARAMETERS
}
