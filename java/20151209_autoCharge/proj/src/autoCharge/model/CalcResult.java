package autoCharge.model;

import autoCharge.code.CalcResultCode;

public class CalcResult {
	private CalcResultCode resultCode;
	private int letChargeCashAmount;
	
	public CalcResult(CalcResultCode code, int letChargeCashAmount){
		this.resultCode = code;
		this.letChargeCashAmount = letChargeCashAmount;
	}

	public CalcResultCode getResultCode() {
		return resultCode;
	}

	public void setResultCode(CalcResultCode resultCode) {
		this.resultCode = resultCode;
	}

	public int getLetChargeCashAmount() {
		return letChargeCashAmount;
	}

	public void setLetChargeCashAmount(int letChargeCashAmount) {
		this.letChargeCashAmount = letChargeCashAmount;
	}
	
	
}
