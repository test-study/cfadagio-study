package autoCharge.model;

import autoCharge.exception.OverChargeException;

public interface Charger {
	public boolean charge(String gameCode, int userNum, int chargeCashAmount) throws Exception;
}
