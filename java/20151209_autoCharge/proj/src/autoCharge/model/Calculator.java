package autoCharge.model;

import autoCharge.code.CalcResultCode;

public class Calculator {
	public CalcResult calculate(int balance, int goalItemCashAmount){
		CalcResult result;
		
		if(balance < 0 || goalItemCashAmount < 0){
			result = new CalcResult(CalcResultCode.INVALID_PARAMETERS, 0);
		} else if(balance > goalItemCashAmount){
			result = new CalcResult(CalcResultCode.ENOUGH_BALANCE, 0);
		} else{
			result = new CalcResult(CalcResultCode.AVAIL_CHARGE, goalItemCashAmount - balance);
		}
		
		return result;
	}
}
