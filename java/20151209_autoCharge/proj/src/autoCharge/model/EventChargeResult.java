package autoCharge.model;

import autoCharge.code.ChargeEventResultCode;

public class EventChargeResult {
	private ChargeEventResultCode resultCode;
	private int chargedCashBonusAmount;
	
	public EventChargeResult(ChargeEventResultCode code, int ccba){
		this.resultCode = code;
		this.chargedCashBonusAmount = ccba;
	}

	public ChargeEventResultCode getResultCode() {
		return resultCode;
	}

	public void setResultCode(ChargeEventResultCode resultCode) {
		this.resultCode = resultCode;
	}

	public int getChargedCashBonusAmount() {
		return chargedCashBonusAmount;
	}

	public void setChargedCashBonusAmount(int chargedCashBonusAmount) {
		this.chargedCashBonusAmount = chargedCashBonusAmount;
	}
	
	
}
