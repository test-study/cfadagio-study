package autoCharge.model;

import autoCharge.code.ChargeResultCode;

public class ChargeResult {
	private ChargeResultCode code;
	private int chargedCashAmount;
	private int chargedCashBonusAmount;
	
	public ChargeResult(ChargeResultCode code, int cca, int ccba){
		this.code = code;
		this.chargedCashAmount = cca;
		this.chargedCashBonusAmount = ccba;
	}

	public ChargeResultCode getCode() {
		return code;
	}

	public void setCode(ChargeResultCode code) {
		this.code = code;
	}

	public int getChargedCashAmount() {
		return chargedCashAmount;
	}

	public void setChargedCashAmount(int chargedCashAmount) {
		this.chargedCashAmount = chargedCashAmount;
	}

	public int getChargedCashBonusAmount() {
		return chargedCashBonusAmount;
	}

	public void setChargedCashBonusAmount(int chargedCashBonusAmount) {
		this.chargedCashBonusAmount = chargedCashBonusAmount;
	}
	
	
}
