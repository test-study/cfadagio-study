package src;

import static org.junit.Assert.*;
import model.AutoChargeTestDaoImpl;

import org.junit.Test;

import autoCharge.code.ChargeEventResultCode;
import autoCharge.model.EventChargeResult;
import autoCharge.service.AutoChargeEventServiceImpl;

public class AutoChargeEventServiceTest {
	@Test
	public void closeTest(){
		AutoChargeEventServiceImpl target = new AutoChargeEventServiceImpl(new AutoChargeTestDaoImpl(false, false, 1));
		
		assertEquals(ChargeEventResultCode.CLOSE_EVENT, target.charge("cb1", 3612166, 1000).getResultCode());
	}
	
	@Test
	public void parameterTest(){
		AutoChargeEventServiceImpl target = new AutoChargeEventServiceImpl(new AutoChargeTestDaoImpl(false, true, 1));
		
		assertEquals(ChargeEventResultCode.INVALID_PARAMETERS, target.charge("cb1", 3612166, -1).getResultCode());
		assertEquals(ChargeEventResultCode.INVALID_PARAMETERS, target.charge("cb1", -3612166, 1).getResultCode());
	}
	
	@Test
	public void shouldApplyRate(){
		int testRate = 3;
		int chargedCashAmount = 5000;
		
		AutoChargeEventServiceImpl target = new AutoChargeEventServiceImpl(new AutoChargeTestDaoImpl(false, true, testRate));
		
		EventChargeResult result = target.charge("cb1", 3612166, chargedCashAmount);
		
		assertEquals(ChargeEventResultCode.SUCCESS, result.getResultCode());
		assertEquals(testRate * chargedCashAmount, result.getChargedCashBonusAmount());
	}
}
