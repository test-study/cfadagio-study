package src;

import static org.junit.Assert.*;

import org.junit.Test;

import autoCharge.code.CalcResultCode;
import autoCharge.model.CalcResult;
import autoCharge.model.Calculator;

public class CalculatorTest {
	@Test
	public void shouldReturnCorrectResult(){
		Calculator target = new Calculator();
		
		assertEquals(CalcResultCode.INVALID_PARAMETERS, target.calculate(-1, 0).getResultCode());
		assertEquals(CalcResultCode.ENOUGH_BALANCE, target.calculate(10000, 8000).getResultCode());
		
		int balance = 500;
		int tryCashItemAmount = 10000;
		CalcResult result = target.calculate(balance, tryCashItemAmount);
		
		assertEquals(CalcResultCode.AVAIL_CHARGE, result.getResultCode());
		assertEquals(tryCashItemAmount - balance, result.getLetChargeCashAmount());
	}
}
