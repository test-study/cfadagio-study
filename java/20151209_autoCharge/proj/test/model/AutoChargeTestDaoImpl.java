package model;

import autoCharge.repository.AutoChargeDao;

public class AutoChargeTestDaoImpl implements AutoChargeDao {
	public boolean autoChargeOpen;
	public boolean autoChargeEventOpen;
	public int rate;
	
	public AutoChargeTestDaoImpl(boolean aco, boolean aceo, int rate){
		this.autoChargeOpen = aco;
		this.autoChargeEventOpen = aceo;
		this.rate = rate;
	}
	public boolean getAutoChargeMaster(String gameCode){
		return this.autoChargeOpen;
	}
	public boolean getAutoChargeEventMaster(String gameCode){
		return this.autoChargeEventOpen;
	}
	public int getEventRate(String gameCode){
		return this.rate;
	}
}
