var ControlLog = require('../model/ControlLog')

function RankDataControlService(rd){
	this.rankDao = rd;
}

RankDataControlService.prototype.searchError = function(){
	return this.rankDao.searchErrorLog();
};

RankDataControlService.prototype.getErrorCount = function(){
	return this.rankDao.getErrorLogCount();
};

exports.RankDataControlService = RankDataControlService;
