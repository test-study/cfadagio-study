exports.ControlLog = function(logIdx, nationCode, content, state, regDateTime){
	this.logIdx = logIdx;
	this.nationCode = nationCode;
	this.content = content;
	this.state = state;
	this.regDateTime = regDateTime; 
	
	return this;
};
