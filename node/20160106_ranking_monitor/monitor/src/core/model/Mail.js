exports.Mail = function(sender, receiver, nationCode, errCnt, content, regDateTime){
	this.sender = sender; 
	this.receiver = receiver; 
	this.nationCode = nationCode;
	this.errCnt = errCnt;
	this.content = content;
	this.regDateTime = regDateTime;

	return this;
};
