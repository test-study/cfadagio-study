var monitorService = require('../core/service/RankMonitorService');
var rankDataService = require('../core/service/RankDataControlService');
var mailModel = require('../core/model/MonitorMail');
var mail = require('../core/model/Mail');
var controlLog = require('../core/model/ControlLog');
var monitorDao = require('../core/dao/MonitorDao');
var rankDao = require('../core/dao/RankDao');
var assert = require('assert');
var sinon = require('sinon');
var expect = require('expect');
var fs = require('fs');
require("date-utils");

describe('Rank monitor service test!(after2)', function(){

	/**
	 * TODO)
	 * - 모니터링 시, 발송되는 메일 내용 테스트
	 * - 모니터링 시, 기록되는 로그 파일 내용 테스트
	 **/

	beforeEach(function () {
		testNationCode = "KR";
		
		monitorDaoStub = new monitorDao.MonitorDao();
		rankDaoStub = new rankDao.RankDao();
		mailer = new mailModel.MonitorMail();
		
		dataService = new rankDataService.RankDataControlService(rankDaoStub);
		rankMonitorService = new monitorService.RankMonitorService(mailer,
				monitorDaoStub, dataService);
    });
	
	it('should send exactly mail if exists error.', function(){
		var errLog = new controlLog.ControlLog(-1, testNationCode, "db connection error", "error", "2016-01-01");
		var errorAllCount = 3;
		
		sinon.stub(rankDaoStub, "searchErrorLog").onFirstCall().returns(errLog);
		sinon.stub(rankDaoStub, "getErrorLogCount").onFirstCall().returns(errorAllCount);
		
		mailerSpy = sinon.spy(mailer, "sendMail");
		
		rankMonitorService.monitor();
		
		sinon.assert.calledOnce(mailerSpy);
		
		sinon.assert.calledWith(mailerSpy, 
				new mail.Mail(rankMonitorService.senderEmailAddress,
										rankMonitorService.receiverEmailAddress, 
										testNationCode, 
										errorAllCount, 
										rankMonitorService.mailContent, 
										null));
	});

	it('should stack local log file if exists error.', function(){
		var errLog = new controlLog.ControlLog("-1", testNationCode, "db connection error", "error", "2016-01-01");
		var errorAllCount = 3;
		var logFileName = "../rank_monitor_log_" + new Date().toFormat("YYYY-MM-DD") + ".txt";
		
		sinon.stub(rankDaoStub, "searchErrorLog").onFirstCall().returns(errLog);
		sinon.stub(rankDaoStub, "getErrorLogCount").onFirstCall().returns(errorAllCount);
		
		rankMonitorService.monitor();
		
		var data = fs.readFileSync(logFileName).toString();
		
		assert.equal( "[" + new Date().toFormat("YYYY-MM-DD HH24:MI:SS") + "] " + errLog.content + errLog.logIdx, data);
	});
});
