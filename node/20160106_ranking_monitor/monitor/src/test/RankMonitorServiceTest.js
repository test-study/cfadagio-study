var monitorService = require('../core/service/RankMonitorService');
var rankDataService = require('../core/service/RankDataControlService');
var mailModel = require('../core/model/MonitorMail');
var monitorDao = require('../core/dao/MonitorDao');
var rankDao = require('../core/dao/RankDao');
var assert = require('assert');
var sinon = require('sinon');
var expect = require('expect');

describe('Rank monitor service test!(before)', function(){
	beforeEach(function () {
    });
	
	it('should save monitor normal log if not exists error.', function(){
		var monitorDaoMock = sinon.mock(new monitorDao.MonitorDao());
		var mailerMock = sinon.mock(new mailModel.MonitorMail());
		var rankDaoMock = sinon.mock(new rankDao.RankDao());
		
		rankDaoMock.expects("searchErrorLog").once().returns(null);
		monitorDaoMock.expects("addNormalLog").once();
		
		var dataService = new rankDataService.RankDataControlService(rankDaoMock.object);
		
		var rankMonitorService = new monitorService.RankMonitorService(mailerMock.object,
				monitorDaoMock.object, dataService);
		
		rankMonitorService.monitor();
		
		monitorDaoMock.verify();
		rankDaoMock.verify();
	});

	it('should save monitor error log if exists error.', function(){
		var monitorDaoMock = sinon.mock(new monitorDao.MonitorDao());
		var mailerMock = sinon.mock(new mailModel.MonitorMail());
		var rankDaoMock = sinon.mock(new rankDao.RankDao());
		
		rankDaoMock.expects("searchErrorLog").once().returns(1);
		monitorDaoMock.expects("addErrorLog").once();
		
		var dataService = new rankDataService.RankDataControlService(rankDaoMock.object);
		
		var rankMonitorService = new monitorService.RankMonitorService(mailerMock.object,
				monitorDaoMock.object, dataService);
		
		rankMonitorService.monitor();
		
		monitorDaoMock.verify();
		rankDaoMock.verify();
	});

	it('should send mail if exists error.', function(){
		var monitorDaoMock = sinon.mock(new monitorDao.MonitorDao());
		var mailerMock = sinon.mock(new mailModel.MonitorMail());
		var rankDaoMock = sinon.mock(new rankDao.RankDao());
		
		rankDaoMock.expects("searchErrorLog").once().returns(1);
		mailerMock.expects("sendMail").once();
		
		var dataService = new rankDataService.RankDataControlService(rankDaoMock.object);
		
		var rankMonitorService = new monitorService.RankMonitorService(mailerMock.object,
				monitorDaoMock.object, dataService);
		
		rankMonitorService.monitor();
		
		rankDaoMock.verify();
		mailerMock.verify();
	});
});

